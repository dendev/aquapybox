## Start

### dev
```bash
export FLASK_APP=app.py && flask run
```
### prod
```bash
gunicorn --bind 0.0.0.0:5000 app:app
```

## Install

### flask
```bash
sudo apt-get install python3-pip
pip3 install Flask 
```

### pi lib
```bash
 pip3 install Rpi.GPIO
```

### config
```bash
cp settings-sample.json settings.json
vim settings.json
```

### submersible temperature sensor
```bash
sudo vim /boot/config.txt
dtoverlay=w1-gpio,gpiopin=4
sudo reboot
```

### like a service
```bash
sudo apt-get install gunicorn3
sudo pip3 install Flask 
sudo pip3 install Rpi.GPIO

chmod u+x run_aquapybox.sh
vim aquapybox.service 
sudo cp aquapybox.service /etc/systemd/system
sudo chmod 644 /etc/systemd/system/aquapybox.service

sudo systemctl start aquapybox
sudo systemctl status aquapybox

sudo systemctl enable aquapybox
```

## GPIO
| device  | type  | pin  | pin  | type  | device |   
|---------|-------|------|------|-------|--------|
|         | 3V    | 1    | 2    | 5V    |        |
|         | I2    | 3    | 4    | 5V    |        |
|         | I3    | 5    | 6    | GND   |        |
|         | I4    | 7    | 8    | I14   |        |
|         | GND   | 9    | 10   | I15   |        |
|         | I17   | 11   | 12   | I18   |        |
|         | I27   | 13   | 14   | GND   |        |
|         | I22   | 15   | 16   | I23   |        |
|         |  3V   | 17   | 18   | I24   |        |
|         | I10   | 19   | 20   | GND   |        |
|         |  I9   | 21   | 22   | I25   |        |
|         | I11   | 23   | 24   | I8    |        |
|         | GND   | 25   | 26   | I7    |        |
|         |       | 27   | 28   |       |        |
|         | I5    | 29   | 30   | GND   |        |
|         | I6    | 31   | 32   | I12   |        |
|         | I13   | 33   | 34   | GND   |        |
|         | I19   | 35   | 36   | I16   |        |
|         | I26   | 37   | 38   | I20   |        |
|         | GND   | 39   | 40   | I21   |        |
