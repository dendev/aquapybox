import sys
sys.path.insert(1, './libs')
from flask import Flask, render_template, jsonify
import json
import task
import fake as relay
import fake as submersible_temperature
#import relay
#import submersible_temperature

# Configs
app = Flask(__name__)
app.config.from_pyfile('config.cfg')
with open('settings.json') as json_file:
    settings = json.load(json_file)

# Tasks
task.run(settings)

# Routes
@app.route('/')
def home(name=None):
    return render_template('home.html', settings=settings)

## register
@app.route('/register')
def register(name=None):
    return jsonify(settings)

## actions
@app.route('/api/<resource_slug>/<action>')
def generic_route(resource_slug=None, action=None):
    response = False
    resource = _check_args(resource_slug, action)

    if( resource ):
        result = _do_action(resource, action)
        if( result ):
            response = _make_response(True, resource, action, 'ok_action_done', result)
        else:
            response = _make_response(False, resource, action, 'ko_action_fail', result)
    else:
        response = _make_response(False, resource, action, 'ko_bad_args', False)

    return response

# Utils
def _check_args(resource_slug, action):
    resources = settings.get('resources')

    ref = next(( item for item in resources if item["slug"] == resource_slug), None )

    if( ref ):
        actions = ref.get('actions')
        if any( a['slug'] == action for a in actions):
            return ref

    return False

def _do_action(resource, action):
    response = False

    type = resource.get('type')
    if( type == 'relay' ):
        response = relay.run(resource.get('pin'), action, resource.get('default') )
    elif( type == 'submersible_temperature' ):
        response = submersible_temperature.run(resource.get('pin'), action)
    elif( type == 'sound_sensor_action' ):
        response = sound_sensor_action.run(resource.get('pin'), resource.get('sub_pin'), action)

    return response

def _get_msg(code):
    msg = False

    if( code == 'ok_action_done'):
        msg = 'Action success'
    elif( code == 'ko_action_fail' ):
        msg = 'Unable to do action'
    elif( code == 'ko_bad_args' ):
        msg = 'Bad args for action'

    return msg

def _make_response(is_success, resource, action, msg_code, result=None):
    msg = _get_msg(msg_code)

    print( result)
    if( resource ):
        infos = { 'resource': resource.get('label'), 'action': action, 'default': resource.get('default'), 'description': resource.get('description')}
    else:
        infos = {}

    return jsonify(success=is_success, infos=infos, msg=msg, result=result)
