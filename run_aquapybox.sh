#!/usr/bin/env bash

readonly PROCESS_NAME="gunicorn"
readonly PROJECT_PATH="/home/pi/aquapybox/"
readonly PROCESS_CMD="gunicorn3 --bind 0.0.0.0:5000 app:app"

if pgrep -x $PROCESS_NAME > /dev/null
then
    echo "It's already running"
else
    echo "Start"
    cd $PROJECT_PATH
    $PROCESS_CMD
fi
