#!/usr/bin/env python3

import sys, getopt
import RPi.GPIO as GPIO

def run(pin, action, default):
    GPIO.setwarnings(False)

    print( 'relay number is: ', pin )
    print( 'relay action is: ', action )

    inverted = True if default == 'on' else False

    GPIO.setmode(GPIO.BCM)
    GPIO.setup(pin, GPIO.OUT)

    if action == 'on':
        GPIO.output(pin, GPIO.HIGH if inverted else GPIO.LOW)
    elif action == 'off':
        GPIO.output(pin, GPIO.LOW if inverted else GPIO.HIGH)
    elif action == 'toggle':
        state = GPIO.input(pin)
        if( state == 0 ):
            GPIO.output(pin, GPIO.HIGH)
        else:
            GPIO.output(pin, GPIO.LOW)
    else:
        if( not action == 'state' ):
            help()

    state = GPIO.input(pin)
    label = 'on' if ( inverted and state == 1 ) or ( not inverted and state == 0 ) else 'off'
    print( 'relay state is : ', state )
    return {'relais': pin, 'value': state, 'label': label}
    #GPIO.cleanup()

def help():
    print( '*Relay usage: ' )
    print( 'run( pin, action )' )
    print( 'available actions : on, off, toggle, state' )
