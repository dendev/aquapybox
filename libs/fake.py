#!/usr/bin/env python3

def run(pin, action, default='off'):
    inverted = True if default == 'on' else False

    state = None
    if action == 'on':
        state =  1 if inverted else 0
    elif action == 'off':
        state = 0 if inverted else 1
    else:
        state = 1

    label = 'on' if ( inverted and state == 1 ) or ( not inverted and state == 0 ) else 'off'
    print( 'relay pin is : ', pin)
    print( 'relay action is : ', action)
    print( 'relay default is : ', default)
    print( 'relay inverted is : ', inverted )
    print( 'relay state is : ', state )
    print( 'relay label is : ', label )
    return {'relais': 1, 'value': state, 'label': label}
