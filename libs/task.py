#!/usr/bin/env python3
from multiprocessing import Process

#import sound_sensor_task
import fake as sound_sensor_task

def run(settings):
    result = False
    resources = settings.get('resources')

    for item in resources:
        if( "-task" in item["type"] ):
            result = _do_action( item )

    return result

def _do_action(item):
    result = False

    if( item.get('slug') == 'sound_sensor-task' ):
        p = Process( target=sound_sensor_task.run, args=(item.get('pin'), item.get('sub_pin') ) )
        p.start()

    return True # bof
