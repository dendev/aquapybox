#!/usr/bin/python3

import RPi.GPIO as GPIO
import time
import relay

def run( pin, sub_pin ):

    GPIO.setmode(GPIO.BCM)
    GPIO.setup(pin, GPIO.IN)

    cb = lambda pin=pin, sub_pin=sub_pin: callback(pin, sub_pin)

    GPIO.add_event_detect(pin, GPIO.BOTH, bouncetime=180)  # let us know when the pin goes HIGH or LOW
    GPIO.add_event_callback(pin, cb)  # assign function to GPIO PIN, Run function on change

    # infinite loop
    while True:
        time.sleep(1)

    #GPIO SETUP

    return True

def callback(pin, sub_pin):
    if GPIO.input( pin ):
        print( "Sound Detected!" )
        relay.run(sub_pin, 'toggle')
    else:
        print( "...." )